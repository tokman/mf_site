'use strict'

/**
 * App entry point.
 *
 * @module App
 */

/** Import initialized-by-default modules/libs */
import Common from './components/Common'
import './components/PublicAPI'
/** Import page controllers */
import Home from './pages/Home'
import Products from './pages/Products'
import Features from './pages/Features'
import About from './pages/About'
import Pricing from './pages/Pricing'
import Popup from 'vintage-popup'

import { currentPage } from './modules/dev/_helpers'

/**
 * Run appropriate scripts for each page.
 **/

window.Popup = Popup
Popup.expose($)
$('[data-popup-target]').popup()
Common.initBody()

switch (currentPage) {
  /** Home page */
  case 'home':
    new Home
    Common.initBody()
    break
  case 'products':
    new Products
    Common.initBody()
    break
  case 'features':
    new Features
    Common.initBody()
    break
  case 'about':
    new About
    Common.initBody()
    break
  case 'pricing':
    new Pricing
    Common.initBody()
    break

  /** No page found */
  default:
    Common.initBody()
    console.warn('Undefined page')
}

