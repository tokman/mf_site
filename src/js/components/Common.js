import { Linear, Power1, Power4, TimelineMax, TweenLite, TweenMax } from 'gsap'
import { $body, $header, $window, css, Resp } from '../modules/dev/_helpers'
import '../modules/dep/DrawSVGPlugin'

export class Common {
  static animClass = '.is_anim'

  /**
   * Cache data, make preparations and initialize common scripts.
   */
  constructor () {
    this.tween = new TimelineMax()
    this.initHeaderAnimation()
    this.initDropdown()

    let tween = new TimelineMax({
      repeat: -1
    })

    this.menuTween = new TimelineMax()

    let secondtween = new TimelineMax()

    const $items = $('input, textarea')

    $items.focus(function () {
      $(this).parent().addClass(css.focus)
      $(this).parent().removeClass(css.error)
    })

    $items.blur(function () {
      if ($(this).val().length === 0) $(this).parent().removeClass(css.focus)
    })

    $('.up_button').click(function () {
      $('html').animate({scrollTop: 0}, 'slow')
    })

    this.initMobileMenu()
    this.initContactForm()
    this.initCtaBlock()

  }

  initCtaBlock () {
    $('.form > .row > input').keypress(function () {
      if (Common.validateEmail($(this).val())) {
        $(this).removeClass('error')
        $(this).addClass('ok')

        $('.icon > .error').removeClass(css.active)
        $('.icon > .ok').addClass(css.active)
        return
      }

      $('.icon > .ok').removeClass(css.active)
      $('.icon > .error').addClass(css.active)

      $(this).removeClass('ok')
      $(this).addClass('error')
    })

    $('.form .btn').click(function () {
      let $email = $('.form > .row > input')
      let email = $email.val()

      if (Common.validateEmail(email)) {
        $email.removeClass('error')
        $email.addClass('ok')

        $('.icon > .error').removeClass(css.active)
        $('.icon > .ok').addClass(css.active)

        $.post(
          'https://new.mailfire.io/index.php',
          {
            email: email,
            type: 'email_form'
          },
          function (response) {
            console.log(response)
          }
        )

        $('.form').html('<div class="thank__you">Thank you!</div><div class="thank__you">Check your inbox - We sent you a letter😊</div><div class="thank__you">Feel free to ask any questions, we’d love to answer them!</div>')

        return
      }

      $('.icon > .ok').removeClass(css.active)
      $('.icon > .error').addClass(css.active)

      $email.removeClass('ok')
      $email.addClass('error')
    })
  }

  initContactForm () {
    let $btn = $('.contact_reqeust_btn')

    $btn.click(function (e) {
      e.preventDefault()

      let name = $('.contact__request .name').val()
      let company = $('.contact__request .company').val()
      let email = $('.contact__request .email').val()

      if (name.length <= 2) {
        $('.contact__request .name').parent().addClass(css.error)
        return false
      }

      if (company.length <= 2) {
        $('.contact__request .company').parent().addClass(css.error)
        return false
      }

      if (!Common.validateEmail(email)) {
        $('.contact__request .email').parent().addClass(css.error)
        return false
      }

      $.post(
        'https://new.mailfire.io/index.php',
        {
          email: email,
          name: name,
          company: company,
          type: 'contact_form'
        },
        function (response) {
          console.log(response)
        }
      )

      $('.request .form_block').html('<div class="thank__you">Thank you!</div><div class="thank__you">Check your inbox - We sent you a letter😊</div><div class="thank__you">Feel free to ask any questions, we’d love to answer them!</div>')
      $('.request .popup__title').remove()

    })
  }

  static validateEmail (email) {
    let re = /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/
    return re.test(String(email).toLowerCase())
  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }

  initAnimation () {
    new TimelineMax().fromTo(['header', '.hamburger'], .6, {opacity: 0, y: -70, backgroundColor: 'transparent'}, {
      position: 'fixed',
      opacity: 1,
      y: 0,
      backgroundColor: 'rgba(255, 255, 255, 1)',
      ease: Power4.easeOut
    })
  }

  /**
   * Init mobile show/hide header animation
   */
  initHeaderAnimation () {
    if (!Resp.isMobile) return
    let lastScrollTop = 100, st

    let $hamburger = $('.hamburger')

    const _this = this

    $window.on('scroll', function (e) {
      if (_this.tween.isActive()) return

      st = $(this).scrollTop()

      if (!_this.animationInited && st > 500) {
        _this.initAnimation()
        _this.animationInited = true
      }

      if (st < 200) {
        $header.attr('style', '')
        $hamburger.attr('style', '')
        _this.animationInited = false
      }

      lastScrollTop = st
    })
  }

  initDropdown () {
    $('.dropdown:eq(0) > .menu_link').hover(function () {
      TweenLite.to($('.sub_menu:eq(0)'), .5, {opacity: 1, zIndex: 2, visibility: 'visible'})
      TweenLite.to($('.sub_menu:eq(1)'), .5, {opacity: 0, zIndex: -1, visibility: 'hidden'})
    }, function () {

    })

    $('li:not(.dropdown) > .menu_link').hover(function () {
      TweenLite.to($('.sub_menu:eq(0)'), .5, {opacity: 0, zIndex: -1,  visibility: 'hidden'})
      TweenLite.to($('.sub_menu:eq(1)'), .5, {opacity: 0, zIndex: -1,  visibility: 'hidden'})
    }, function () {

    })

    $('.dropdown:eq(1) > .menu_link').hover(function () {
      TweenLite.to($('.sub_menu:eq(0)'), .5, {opacity: 0, zIndex: -1,  visibility: 'hidden'})
      TweenLite.to($('.sub_menu:eq(1)'), .5, {opacity: 1, zIndex: 2,  visibility: 'visible'})
    }, function () {

    })

    $('.sub_menu').hover(function () {
    }, function () {
      TweenLite.to($('.sub_menu:eq(0)'), .5, {opacity: 0, zIndex: -1,visibility: 'hidden'})
      TweenLite.to($('.sub_menu:eq(1)'), .5, {opacity: 0, zIndex: -1,visibility: 'hidden'})
    })

  }

  /**
   * Set body opacity
   */
  initBody () {
    new TimelineMax({delay: 1}).fromTo(Common.animClass, .5, {opacity: 0}, {opacity: 1})
  }

  initMobileMenu () {
    if (Resp.isDesk) return

    let _this = this

    let $mobileBtn = $('.hamburger')
    let $mobileMenu = $('.mobile__menu')

    $mobileBtn.click(function () {
      if (_this.menuTween.isActive()) return

      $(this).toggleClass('hamburger--spin js-hamburger is-active')

      $body.toggleClass(css.overflow)

      let tween = new TimelineMax()

      let $this = $mobileMenu

      $this.toggleClass(css.open)

      $this.parent().toggleClass(css.open)

      if ($this.hasClass(css.open)) {
        if (_this.menuTween.reversed()) {
          _this.menuTween.play()
          return
        }

        _this.menuTween
        .to($mobileMenu, 0.2, {right: 0, ease: Power1.easeOut})
        .staggerFromTo('.mobile__menu  li', 0.2, {x: 35, opacity: 0,}, {
          x: 0, opacity: 1, ease: Power1.easeOut,
        }, 0.1, 0.2)
      }
      else {
        _this.menuTween.reverse()
      }
    })
  }

}

/** Export initialized common scripts by default */
export default new Common()
