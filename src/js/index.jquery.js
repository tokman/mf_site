// jscs:disable
import 'jquery';

$.fn.isInViewport = function() {
  let elementTop = $(this).offset().top ;
  let elementBottom = elementTop + $(this).outerHeight();

  let viewportTop = $(window).scrollTop() - 10;
  let viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};