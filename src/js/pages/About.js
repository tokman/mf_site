import { Linear, Power1, Power4, TimelineMax, TweenLite } from 'gsap'
import { $window, css, isScrolledIntoView, Resp } from '../modules/dev/_helpers'

export default class About {
  constructor () {
    if (Resp.isMobile) return
    this.section = 'section'

    this.firstScreen = '.who_we_are'
    this.firstTween = new TimelineMax({delay: .9})

    this.secondScreen = '.work_with_us'
    this.secondTween = new TimelineMax({delay: .9})

    this.init()
  }

  init () {
    let _this = this

    _this.initFirstScreen()

    $window.on('scroll', function (e) {
      $(_this.section).each(function () {
        let index = $(_this.section).index(this)

        if (isScrolledIntoView($(_this.secondScreen))) {
          if (isScrolledIntoView($(this)) && index === 1) {
            _this.initSecondScreen()
          }
        }
      })
    })
  }

  initFirstScreen () {
    this.firstTween
    .fromTo(`header`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.firstScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.firstScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15).fromTo(`${this.firstScreen} .img`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)

  }

  initSecondScreen () {
    if ($(`${this.secondScreen}`).hasClass(css.finished) || this.secondTween.isActive()) return

    this.secondTween
    .to(`${this.secondScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.secondScreen} h2`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.secondScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.secondScreen } .row`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.4)
    .fromTo(`${this.secondScreen } .line_arrows`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.secondScreen } .btn`, .9, {x: 120, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.secondScreen} .bg`, .8, {x: -220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .addCallback(() => {
      $(this.secondScreen).addClass(css.finished)
    })

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.secondScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)

    let $screen = $(this.secondScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)
        _this.setParallax(item, pageX, 50, pageY, 10)

      })
    })
  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }
}
