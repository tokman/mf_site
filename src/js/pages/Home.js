/**
 * Home page scripts.
 *
 * @module Home
 */

import { $window, css, isScrolledIntoView, Resp, $document } from '../modules/dev/_helpers'
import { Linear, Power1, Power4, TimelineMax, TweenLite } from 'gsap'

export default class Home {
  /**
   * Cache data, make preparations and initialize page scripts.
   */
  constructor () {
    if (Resp.isMobile) return
    this.section = 'section'

    this.firstTween = new TimelineMax()
    this.firstScreen = '.first_screen'

    this.secondScreen = '.second_screen'
    this.secondTween = new TimelineMax().set([`${this.secondScreen}`], {opacity: 0})

    this.contetItems = '.products_list > div'

    this.thirdScreen = '.third_screen'
    this.thirdTween = new TimelineMax().set([`${this.thirdScreen}`], {opacity: 0})

    this.thirdScreenContentItmes = '.list > div'

    this.fourthScreen = '.trustworthy'
    this.fourthTween = new TimelineMax().set([`${this.fourthScreen}`], {opacity: 0})

    this.fourthContentItems = '.items'

    this.sliderScreen = '.slider_screen'
    this.sliderTween = new TimelineMax().set([`${this.sliderScreen}`], {opacity: 0})

    this.newsScreen = '.news_block'
    this.newsTween = new TimelineMax().set([`${this.newsScreen}`], {opacity: 0})

    this.ctaScreen = '.cta_block'
    this.ctaTween = new TimelineMax().set([`${this.ctaScreen}`], {opacity: 0})

    this.init()


  }

  /**
   * Initialize Home page scripts.
   */
  init () {
    let _this = this

    this.initFirstScreen()

    $window.on('scroll', function (e) {
      $(_this.section).each(function () {
        let index = $(_this.section).index(this)

        if (isScrolledIntoView($(_this.secondScreen))) {
          let index = $(_this.section).index(this)
          if (isScrolledIntoView($(this)) && index === 1) {
            _this.initSecondScreen()
          }
        }
        if (isScrolledIntoView($(_this.thirdScreen), -50)) {
          if (isScrolledIntoView($(this)) && index === 2) {
            _this.initThirdScreen()
          }
        }
        // if (isScrolledIntoView($(_this.fourthScreen))) {
        //   if (isScrolledIntoView($(this)) && index === 3) {
        //     _this.initFourthScreen()
        //   }
        // }
        // if (isScrolledIntoView($(_this.sliderScreen))) {
        //   if (isScrolledIntoView($(this)) && index === 4) {
        //     _this.initSliderScreen()
        //   }
        // }
        // if (isScrolledIntoView($(_this.newsScreen))) {
        //   if (isScrolledIntoView($(this)) && index === 5) {
        //     _this.initNewsScreen()
        //   }
        // }

        if (isScrolledIntoView($(_this.ctaScreen))) {
          if (isScrolledIntoView($(this))) {
            _this.initCtaScreen()
          }
        }
      })
    })

    let $leftButton = $('.left_button')
    let $rightButton = $('.right_button')
    let $slides = $('.slide')

    $leftButton.click(function () {
      let index = $('.slide.active').index()

      console.log('here left')

      if (!index) return
    })

    $document.on('click', '.right_button', function () {
      console.log('here right')
    })

    // $rightButton.click(function () {
    //   let index = $('.slide.active').index()
    //
    //   console.log('here right')
    //
    //   // new TimelineMax().fromTo('.slide.active', .6, {y: 0, opacity: 1}, {
    //   //   y: -20,
    //   //   opacity: 0,
    //   //   ease: Power1.easeOut,
    //   // }, .55)
    //   //
    //   // new TimelineMax().fromTo($(`.slide:eq(${index + 1})`), .6, {y: -20, opacity: 0}, {
    //   //   y: 0,
    //   //   opacity: 1,
    //   //   ease: Power1.easeOut,
    //   // }, .55)
    //
    //   $slides.removeClass(css.active)
    //   $(`.slide:eq(${index + 1})`).addClass(css.active)
    //
    // })
  }

  initFirstScreen () {
    this.firstTween
    .fromTo(`header`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} .left`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.firstScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.firstScreen} .first_circle`, 2, {y: -75, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .second_circle`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .8)
    .fromTo(`${this.firstScreen} .third_circle`, 2, {x: -45, y: -10, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, 1)
    .fromTo(`${this.firstScreen} .fourth_circle`, 2, {y: 45, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .five_circle`, 2, {x: 50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .six_circle`, 2, {x: -50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .seven_circle`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen } .line_arrows`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.firstScreen } .btn`, .9, {x: 120, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.firstScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)

    this.initFirstScreenParallax()
  }

  initSecondScreen () {
    if ($(`${this.secondScreen}`).hasClass(css.finished) || this.secondTween.isActive()) return

    this.secondTween
    .to(`${this.secondScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.secondScreen} h1`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .55)
    .fromTo(
      `${this.secondScreen } p`
      , .6, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .8)
    .staggerFromTo(this.contetItems, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1)
    .addCallback(() => $(this.secondScreen).addClass(css.finished))
  }

  initThirdScreen () {
    if ($(`${this.thirdScreen}`
    ).hasClass(css.finished) || this.thirdTween.isActive()) return

    this.initThirdScreenParallax()

    this.thirdTween
    .to(`${this.thirdScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.thirdScreen} h1`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .fromTo(`${this.thirdScreen } .description`
      , .6, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, 1.2)
    .staggerFromTo(this.thirdScreenContentItmes, 0.75, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, .7)
    .fromTo(`${this.thirdScreen } .line_arrows`
      , .6, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .7)
    .fromTo(`${this.thirdScreen } .btn`, .9, {x: 120, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.thirdScreen} .first_circle`, 2, {x: -75, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.thirdScreen} .second_circle`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .8)
    .fromTo(`${this.thirdScreen} .third_circle`, 2, {x: -45, y: -10, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, 1)
    .fromTo(`${this.thirdScreen} .fourth_circle`, 2, {y: 45, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.thirdScreen} .five_circle`, 2, {x: 50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.thirdScreen} .six_circle`, 2, {x: -50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.thirdScreen} .item`, 2.5, {y: -45, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .addCallback(() => {
      $(this.thirdScreen).addClass(css.finished)
    })

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.thirdScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)
  }

  initFourthScreen () {
    if ($(`${this.fourthScreen}`).hasClass(css.finished) || this.fourthTween.isActive()) return

    this.initThirdScreenParallax()

    this.fourthTween
    .to(`${this.fourthScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(
      `${this.fourthScreen} h1`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .65)
    .fromTo(
      `${this.fourthScreen } p`
      , .6, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .8)
    .fromTo(
      `${this.fourthScreen } .item`
      , .6, {scale: 0}, {
        scale: 1,
        ease: Power1.easeOut,
      }, 1)
    .fromTo(
      `${this.fourthScreen } .second_item`
      , .6, {scale: 0}, {
        scale: 1,
        ease: Power1.easeOut,
      }, 1.2)
    .staggerFromTo(this.fourthContentItems, .6, {scale: 0, ease: Power1.easeOut}, {
      scale: 1,
      ease: Power1.easeOut,
    }, 0.2, 1.4)
    .addCallback(() => {
      $(this.fourthScreen).addClass(css.finished)
    })

    this.initFourthScreenParallax()
  }

  initThirdScreenParallax () {
    let $screen = $(this.thirdScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)

        if (item.hasClass('first_circle')) {
          _this.setParallax(item, pageX, 50, pageY, 15)
        }
        else if (item.hasClass('six_circle')) {
          _this.setParallax(item, pageX, -65, pageY, 85)
        }
        else if (item.hasClass('third_circle')) {
          _this.setParallax(item, pageX, 25, pageY, -25)
        }
        else if (item.hasClass('five_circle')) {
          _this.setParallax(item, pageX, 65, pageY, 85)
        }
        else if (item.hasClass('second_circle')) {
          _this.setParallax(item, pageX, -65, pageY, -35)
        }
        else if (item.hasClass('fourth_circle')) {
          _this.setParallax(item, pageX, -65, pageY, 35)
        }
        else if (item.hasClass('item')) {
          _this.setParallax(item, pageX, 10, pageY, -25)
        }
        else {
          _this.setParallax(item, pageX, 50, pageY, 75)
        }
      })
    })
  }

  initFourthScreenParallax () {
    let $screen = $(this.fourthScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
        let pageX = event.pageX - ($screen.width() * 0.5)
        let pageY = event.pageY - ($screen.height() * 0.5)

        $parallaxItems.each(function () {
          let item = $(this)

          if (item.hasClass('item')) {
            _this.setParallax(item, pageX, 50, pageY, 5)
          }
          else if (item.hasClass('second_item')) {
            _this.setParallax(item, pageX, -35, pageY, -10)
          }
        })

        $(
          `${_this.fourthContentItems}}`
        ).each(function (index) {
          if (index % 2) {
            _this.setParallax($(this), pageX, 35, pageY, -5)
          } else {
            _this.setParallax($(this), pageX, -35, pageY, -20)
          }
        })
      }
    )
  }

  initSliderScreen () {
    if ($(`${this.sliderScreen}`).hasClass(css.finished) || this.sliderTween.isActive()) return

    this.sliderTween
    .to(`${this.sliderScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.sliderScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .3)
    .fromTo($(`${this.sliderScreen} .slide:eq(0)`), .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .55)
    .staggerFromTo(`${this.sliderScreen} .slide  div`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2, 1)
    .addCallback(() => {
      $(this.sliderScreen).addClass(css.finished)
    })

  }

  initNewsScreen () {
    if ($(`${this.newsScreen}`).hasClass(css.finished) || this.newsTween.isActive()) return

    this.newsTween
    .to(`${this.newsScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.newsScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .85)
    .fromTo(`${this.newsScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1)
    .staggerFromTo(`${this.newsScreen}  .news_item`, .9, {y: -70, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2, 1.4)
    .addCallback(() => {
      $(this.newsScreen).addClass(css.finished)
    })
  }

  initCtaScreen () {
    if ($(`${this.ctaScreen}`).hasClass(css.finished) || this.ctaTween.isActive()) return

    this.initCtaParallax()

    this.ctaTween
    .to(`${this.ctaScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.ctaScreen} h2`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .85)
    .fromTo(`${this.ctaScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.2)
    .fromTo(`${this.ctaScreen } .row`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.4)
    .fromTo(`${this.ctaScreen } .line_arrows`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.ctaScreen } .btn`, .9, {x: 120, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.2)
    .fromTo(`${this.ctaScreen } .cta`, .9, {y: 120, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .addCallback(() => {
      $(this.ctaScreen).addClass(css.finished)
    })

    new TimelineMax({delay: .9, repeat: -1}).staggerFromTo(`${this.ctaScreen } .items`, .75, {y: 20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .8)
    .addCallback(() => {
      $(this.ctaScreen).addClass(css.finished)
    })

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.ctaScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)
  }

  initCtaParallax () {
    let $screen = $(this.ctaScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)
        _this.setParallax(item, pageX, 50, pageY, 10)

      })
    })
  }

  initFirstScreenParallax () {

    let $screen = $(`${this.firstScreen}`)
    let $parallaxItems = $screen.find('.parallax')

    let _thus = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)

        if (item.hasClass('first_circle')) {
          _thus.setParallax(item, pageX, 50, pageY, -55)
        }
        else if (item.hasClass('six_circle')) {
          _thus.setParallax(item, pageX, -65, pageY, 85)
        }
        else if (item.hasClass('third_circle')) {
          _thus.setParallax(item, pageX, 25, pageY, -25)
        }
        else if (item.hasClass('five_circle')) {
          _thus.setParallax(item, pageX, 65, pageY, 85)
        }
        else if (item.hasClass('second_circle')) {
          _thus.setParallax(item, pageX, -65, pageY, -35)
        }
        else {
          _thus.setParallax(item, pageX, 50, pageY, 75)
        }
      })
    })
  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }
}
