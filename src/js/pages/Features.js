import { Linear, Power1, Power4, TimelineMax, TweenLite } from 'gsap'
import { $window, css, isScrolledIntoView, Resp } from '../modules/dev/_helpers'
import 'hammerjs'

export default class Features {
  constructor () {
    if (Resp.isMobile) {
      this.initStepsSlider()
      return
    }
    this.section = 'section'

    this.firstScreen = '.first_screen'
    this.firstTween = new TimelineMax({delay: .9})

    this.secondScreen = '.create_and_manage'
    this.secondTween = new TimelineMax({delay: 1.2}).set([`${this.secondScreen}`], {opacity: 0})

    this.featuresItems = '.features_items'
    this.featuresTween = new TimelineMax({delay: .9}).set([`${this.featuresItems}`], {opacity: 0})

    this.deliverabilityScreen = '.deliverability'
    this.deliverabilityTween = new TimelineMax({delay: .9}).set([`${this.deliverabilityScreen}`], {opacity: 0})

    this.splitsScreen = '.splits'
    this.splitsTween = new TimelineMax({delay: .9}).set([`${this.splitsScreen}`], {opacity: 0})

    this.stepsScreen = '.steps'
    this.stepsTween = new TimelineMax({delay: .9}).set([`${this.stepsScreen}`], {opacity: 0})

    this.init()
  }

  init () {
    let _this = this

    _this.initFirstScreen()
    _this.initSecondScreen()

    $window.on('scroll', function (e) {
      $(_this.section).each(function () {
        let index = $(_this.section).index(this)

        if (isScrolledIntoView($(_this.secondScreen))) {
          if (isScrolledIntoView($(this)) && index === 1) {
            _this.initSecondScreen()
          }
        }
        if (isScrolledIntoView($(_this.featuresItems))) {
          _this.initFeaturesItems()
        }

        if (isScrolledIntoView($(_this.deliverabilityScreen))) {
          if (isScrolledIntoView($(this)) && index === 2) {
            _this.initDeliverabilityScreen()
          }
        }
        if (isScrolledIntoView($(_this.splitsScreen))) {
          if (isScrolledIntoView($(this)) && index === 3) {
            _this.initSplitsScreen()
          }
        }

        if (isScrolledIntoView($(_this.stepsScreen))) {
          if (isScrolledIntoView($(this)) && index === 4) {
            _this.initStepsScreen()
          }
        }
      })
    })
  }

  initFirstScreen () {
    this.firstTween
    .fromTo(`header`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.firstScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.firstScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15).fromTo(`${this.firstScreen} .first_circle`, 2, {y: -25, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .second_circle`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .8)

    this.initFirstScreenParallax()

  }

  initSecondScreen () {
    if ($(`${this.secondScreen}`).hasClass(css.finished) || this.secondTween.isActive()) return

    this.secondTween
    .to(`${this.secondScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.secondScreen} .info_block`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .addCallback(() => $(this.secondScreen).addClass(css.finished))
  }

  initFeaturesItems () {
    if ($(`${this.featuresItems}`).hasClass(css.finished) || this.featuresTween.isActive()) return

    this.featuresTween
    .to(`${this.featuresItems}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .staggerFromTo(`${this.featuresItems} >  .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.3)
    .addCallback(() => $(this.featuresItems).addClass(css.finished))
  }

  initDeliverabilityScreen () {
    if ($(`${this.deliverabilityScreen}`).hasClass(css.finished) || this.deliverabilityTween.isActive()) return

    this.deliverabilityTween
    .to(`${this.deliverabilityScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.deliverabilityScreen} .info_block`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .staggerFromTo(`${this.deliverabilityScreen} > .delivery_items  > .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.3)
    .addCallback(() => $(this.deliverabilityScreen).addClass(css.finished))
  }

  initSplitsScreen () {
    if ($(`${this.splitsScreen}`).hasClass(css.finished) || this.splitsTween.isActive()) return

    this.splitsTween
    .to(`${this.splitsScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.splitsScreen} .info_block`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .staggerFromTo(`${this.splitsScreen} > .split_items  > .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.3)
    .addCallback(() => $(this.splitsScreen).addClass(css.finished))
  }

  initStepsScreen () {
    if ($(`${this.stepsScreen}`).hasClass(css.finished) || this.stepsTween.isActive()) return

    this.stepsTween
    .to(`${this.stepsScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.stepsScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.stepsScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.stepsScreen} .line_arrows`
      , .6, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .7)
    .fromTo(`${this.stepsScreen } .btn`
      , .9, {x: 120, opacity: 0}, {
        x: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .7)
    .fromTo(`${this.stepsScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .staggerFromTo(`${this.stepsScreen} > .steps_items  > .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.3)
    .fromTo(`${this.stepsScreen } .additional`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.9)
    .fromTo(`${this.stepsScreen} .first_circle`, 2, {y: -75, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.stepsScreen} .second_circle`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .8)
    .fromTo(`${this.stepsScreen} .third_circle`, 2, {x: -45, y: -10, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, 1)
    .fromTo(`${this.stepsScreen} .fourth_circle`, 2, {y: 45, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.stepsScreen} .five_circle`, 2, {x: 50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.stepsScreen} .six_circle`, 2, {x: -50, y: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.stepsScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)
    .addCallback(() => $(this.stepsScreen).addClass(css.finished))

    this.initStepsParallax()
  }

  initFirstScreenParallax () {
    let $screen = $(this.firstScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)

        if (item.hasClass('first_circle')) {
          _this.setParallax(item, pageX, 50, pageY, 15)
        }
        else if (item.hasClass('second_circle')) {
          _this.setParallax(item, pageX, 35, pageY, -35)
        }
        else {
          _this.setParallax(item, pageX, 20, pageY, 75)
        }
      })
    })
  }

  initStepsParallax () {
    let $screen = $(this.stepsScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)

        if (item.hasClass('first_circle')) {
          _this.setParallax(item, pageX, 50, pageY, 5)
        }
        else if (item.hasClass('six_circle')) {
          _this.setParallax(item, pageX, -65, pageY, 25)
        }
        else if (item.hasClass('third_circle')) {
          _this.setParallax(item, pageX, 25, pageY, -25)
        }
        else if (item.hasClass('five_circle')) {
          _this.setParallax(item, pageX, 65, pageY, 25)
        }
        else if (item.hasClass('second_circle')) {
          _this.setParallax(item, pageX, -65, pageY, -15)
        }
        else if (item.hasClass('fourth_circle')) {
          _this.setParallax(item, pageX, -65, pageY, 15)
        }
        else if (item.hasClass('item')) {
          _this.setParallax(item, pageX, 10, pageY, -25)
        }
        else {
          _this.setParallax(item, pageX, 50, pageY, 15)
        }
      })
    })
  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }

  initStepsSlider () {
    this.sliderTween = new TimelineMax()
    this.$toolbar = $('.mobile_steps')

    let $stepsNav = $('.stesp__nav')

    let manager = new Hammer(this.$toolbar[0])
    let step = 1
    manager.get('swipe').set({direction: Hammer.DIRECTION_HORIZONTAL, threshold: 30})
    manager.on('swipeleft', function (e) {
      e.preventDefault()
      let index = $('.steps___nav.active').index()

      let x = (index + 1) * -263

      if (index <= 4) {

        if(index === 4) {
          x = x + 93;
        }

        let tween = new TimelineMax()
        tween.to('.mobile_steps', 0.4, {
          x: x,
          ease: Power4.easeOut,
        })

        $('.steps___nav').removeClass(css.active)

        console.log(index)

        $(`.steps___nav:eq(${index + 1})`).addClass(css.active)
      }

    })

    manager.on('swiperight', function (e) {
      e.preventDefault()
      let index = $('.steps___nav.active').index()

      let x = (index - 1) * -263

      if (index !== 0) {
        let tween = new TimelineMax()
        tween.to('.mobile_steps', 0.4, {
          x: x,
          ease: Power4.easeOut,
        })

        $('.steps___nav').removeClass(css.active)

        $(`.steps___nav:eq(${index - 1})`).addClass(css.active)
      }
    })
  }
}
