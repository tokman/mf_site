import { Linear, Power1, Power4, TimelineMax, TweenLite } from 'gsap'
import { $window, css, isScrolledIntoView, Resp } from '../modules/dev/_helpers'
import { Common } from '../components/Common'

export default class Pricing {
  constructor () {
    if (Resp.isMobile || Resp.isTablet) {
      this.$arrow = $('.arrow')
      this.$priceData = $('.price_data')

      this.$arrow.click(function () {
        let $parent = $(this).parent().parent()
        let index = $('.arrow').index(this)

        if ($parent.hasClass(css.active)) {
          $(`.price_data:eq(${index})`).slideUp()
        } else {
          $(`.price_data:eq(${index})`).slideDown()
        }

        if ($parent.hasClass(css.active)) {
          setTimeout(() => {
            $parent.toggleClass(css.active)
          }, 500)
        } else {
          $parent.toggleClass(css.active)
        }

      })

      return
    }
    this.section = 'section'

    this.firstScreen = '.firstscreen'
    this.firstTween = new TimelineMax({delay: .9})

    this.pricingContent = '.pricing_block > .content'
    this.pricingTween = new TimelineMax({delay: .9})

    this.ctaScreen = '.cta_block'
    this.ctaTween = new TimelineMax({delay: .9}).set([`${this.ctaScreen}`], {opacity: 0})

    const $items = $('input, textarea')

    $items.focus(function () {
      $(this).parent().addClass(css.focus)
      $(this).parent().removeClass(css.error)
    })

    $items.blur(function () {
      if ($(this).val().length === 0) $(this).parent().removeClass(css.focus)
    })

    this.init()
    this.initPriceForm()
  }

  initPriceForm () {
    $('.price__popup .btn').click(function (e) {
      e.preventDefault()

      let name = $(this).parent().find('.name').val()
      let company = $(this).parent().find('.company').val()
      let email = $(this).parent().find('.email').val()
      let type = $(this).parent().find('.package_type').val()

      if (name.length <= 2) {
        $(this).parent().find('.name').parent().addClass(css.error)
        return false
      }

      if (company.length <= 2) {
        $$(this).parent().find('.company').parent().addClass(css.error)
        return false
      }

      if (!Common.validateEmail(email)) {
        $(this).parent().find('.email').parent().addClass(css.error)
        return false
      }



      $.post(
        'https://new.mailfire.io/index.php',
        {
          email: email,
          name: name,
          company: company,
          package: type,
          type: 'price_form'
        },
        function (response) {
          console.log(response)
        }
      )

      $(this).parent().parent().parent().html('<div class="thank__you">Thank you!</div><div class="thank__you">Check your inbox - We sent you a letter😊</div><div class="thank__you">Feel free to ask any questions, we’d love to answer them!</div>')
      $('.request .popup__title').remove()
    })
  }

  init () {
    let _this = this

    _this.initFirstScreen()
    _this.initPricingTween()

    $window.on('scroll', function (e) {
      $(_this.section).each(function () {
        if (isScrolledIntoView($(_this.ctaScreen))) {
          if (isScrolledIntoView($(this))) {
            _this.initCtaScreen()
          }
        }
      })
    })
  }

  initFirstScreen () {
    this.firstTween
    .fromTo(`header`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} .title`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.firstScreen } .subtitle`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.firstScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} .first_item`, 2, {y: -75, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .img`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)

    this.initFirstScreenParallax()
  }

  initFirstScreenParallax () {
    let $screen = $(this.firstScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)
        _this.setParallax(item, pageX, 50, pageY, 10)

      })
    })
  }

  initPricingTween () {
    if ($(`${this.pricingContent}`).hasClass(css.finished) || this.pricingTween.isActive()) return

    this.pricingTween.fromTo(`${this.pricingContent} .title`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.pricingContent } .subtitle`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .addCallback(() => $(this.pricingContent).addClass(css.finished))
  }

  initCtaScreen () {
    if ($(`${this.ctaScreen}`).hasClass(css.finished) || this.ctaTween.isActive()) return

    this.ctaTween
    .to(`${this.ctaScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.ctaScreen} h2`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.ctaScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.ctaScreen } .row`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, 1.4)
    .fromTo(`${this.ctaScreen } .line_arrows`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.ctaScreen } .btn`, .9, {x: 120, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .7)
    .fromTo(`${this.ctaScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .addCallback(() => $(this.ctaScreen).addClass(css.finished))

    new TimelineMax({
      repeat: -1
    }).staggerFromTo(`${this.ctaScreen} .nav0`, .3, {opacity: 0, ease: Power1.easeOut}, {
      opacity: 1,
      ease: Power1.easeOut,
    }, 0.2)

  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }
}
