import { $window, css, isScrolledIntoView, Resp } from '../modules/dev/_helpers'
import { Linear, Power1, Power4, TimelineMax, TweenLite } from 'gsap'
import '../modules/dep/DrawSVGPlugin'

export default class Products {

  constructor () {
    if (Resp.isMobile) return
    this.section = 'section'

    this.firstTween = new TimelineMax({delay: .9})
    this.firstScreen = '.info_block'

    this.secondTween = new TimelineMax
    this.secondScreen = '.campaigns'

    this.featuresScreen = '.features'
    this.featuresTween = new TimelineMax().set([`${this.featuresScreen}`], {opacity: 0})
    this.featuresScreen = '.features'

    this.webPushScreen = '.web_push'
    this.webPushTween = new TimelineMax().set([
      $(`${this.webPushScreen} circle`)
    ], {fill: '#A9ADC6'})

    this.webPushTween.set([`${this.webPushScreen}`], {opacity: 0})

    this.webPushTween.set([
      $(`${this.webPushScreen} circle:eq(1)`)
    ], {fill: '#273271'})

    this.pushFeaturesScreen = '.push_features'
    this.pushFeaturesTween = new TimelineMax().set([`${this.pushFeaturesScreen}`], {opacity: 0})

    this.appPushScreen = '.app_push'
    this.appPushTween = new TimelineMax().set([
      $(`${this.appPushScreen} circle`)
    ], {fill: '#A9ADC6'})

    this.appPushTween.set([`${this.appPushScreen}`], {opacity: 0})

    this.appPushTween.set([
      $(`${this.appPushScreen} circle:eq(2)`)
    ], {fill: '#273271'})

    this.appScreen = '.app'
    this.appTween = new TimelineMax().set([
      $(`${this.appScreen} circle`)
    ], {fill: '#A9ADC6'})

    this.appTween.set([
      $(`${this.appScreen} circle:eq(3)`)
    ], {fill: '#273271'})

    this.appTween.set([`${this.appScreen}`], {opacity: 0})

    this.appPushTween.set([
      `${this.apiScreen} h2`,
      `${this.apiScreen} .item`
    ], {opacity: 0})

    this.appFeaturesSreen = '.app_features'
    this.appFeaturesTween = new TimelineMax().set([`${this.appFeaturesSreen}`], {opacity: 0})

    this.apiScreen = '.api'
    this.apiTween = new TimelineMax().set([`${this.apiScreen}`], {opacity: 0})

    this.init()
  }

  init () {
    let _this = this

    _this.initFirstScreen()
    _this.initSecondScreen()

    $window.on('scroll', function (e) {
      $(_this.section).each(function () {
        let index = $(_this.section).index(this)

        if (isScrolledIntoView($(_this.secondScreen))) {
          if (isScrolledIntoView($(this)) && index === 1) {
            _this.initSecondScreen()
          }
        }
        if (isScrolledIntoView($(_this.featuresScreen))) {
          _this.initFeatures()
        }
        if (isScrolledIntoView($(_this.webPushScreen))) {
          if (isScrolledIntoView($(this)) && index === 2) {
            _this.initWebPushScreen()
          }
        }

        if (isScrolledIntoView($(_this.pushFeaturesScreen)) && index === 3) {
          _this.initPushFeatures()
        }

        if (isScrolledIntoView($(_this.appPushScreen))) {
          if (isScrolledIntoView($(this)) && index === 4) {
            _this.initAppPushScreen()
          }
        }

        if (isScrolledIntoView($(_this.appScreen))) {
          if (isScrolledIntoView($(this)) && index === 5) {
            _this.initAppScreen()
          }
        }

        if (isScrolledIntoView($(_this.appFeaturesSreen))) {
          _this.initAppFeatures()
        }

        if (isScrolledIntoView($(_this.apiScreen))) {
          if (isScrolledIntoView($(this)) && index === 6) {
            _this.initApiScreen()
          }
        }

      })
    })
  }

  initFirstScreen () {
    this.firstTween
    .fromTo(`header`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} .left`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} h1`, .7, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .35)
    .fromTo(`${this.firstScreen } p`, .6, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .5)
    .fromTo(`${this.firstScreen} svg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .fromTo(`${this.firstScreen} .first_item`, 2, {y: -75, opacity: 0, ease: Power1.easeOut}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .5)
    .fromTo(`${this.firstScreen} .second_item`, 2, {x: 45, opacity: 0, ease: Power1.easeOut}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut
    }, .8)

    this.initFirstScreenParallax()
  }

  initFirstScreenParallax () {
    let $screen = $(this.firstScreen)
    let $parallaxItems = $screen.find('.parallax')
    let _this = this

    $screen.on('mousemove', (event) => {
      let pageX = event.pageX - ($screen.width() * 0.5)
      let pageY = event.pageY - ($screen.height() * 0.5)

      $parallaxItems.each(function () {
        let item = $(this)

        if (item.hasClass('first_item')) {
          _this.setParallax(item, pageX, 50, pageY, 15)
        }
        else if (item.hasClass('second_item')) {
          _this.setParallax(item, pageX, -65, pageY, 85)
        }
        else {
          _this.setParallax(item, pageX, 50, pageY, 75)
        }
      })
    })
  }

  initSecondScreen () {
    if ($(`${this.secondScreen}`).hasClass(css.finished) || this.secondTween.isActive()) return

    this.secondTween.fromTo(`${this.secondScreen} h2`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .staggerFromTo(`${this.secondScreen} .sub_info div`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.3)
    .fromTo(`${this.secondScreen} svg`, .8, {x: -220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .staggerFromTo(`${this.secondScreen} .nav_item`, 0.95, {
      opacity: 0,
      width: 0,
    }, {
      opacity: 1,
      width: 320,
      ease: Power1.easeOut,
    }, 0.5, 1.3)
    .staggerFromTo(`${this.secondScreen} circle`, 0.65, {
      opacity: 0,
      attr: {
        r: 0,
      }
    }, {
      opacity: 1,
      attr: {
        r: 7,
      },
      ease: Power1.easeOut,
    }, 0.5, 1.3)
    .addCallback(() => $(this.secondScreen).addClass(css.finished))

  }

  initFeatures () {
    if ($(`${this.featuresScreen}`).hasClass(css.finished) || this.featuresTween.isActive()) return

    this.featuresTween.to(`${this.featuresScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .staggerFromTo(`${this.featuresScreen}  .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, .8)
    .addCallback(() => $(this.featuresScreen).addClass(css.finished))
  }

  initWebPushScreen () {
    if ($(`${this.webPushScreen}`).hasClass(css.finished) || this.webPushTween.isActive()) return

    this.webPushTween.to(`${this.webPushScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.webPushScreen} h2`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .65)
    .staggerFromTo(`${this.webPushScreen} .info_block div`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1.1)
    .fromTo(`${this.webPushScreen} svg:eq(1)`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .staggerFromTo(`${this.webPushScreen} .nav_item`, 0.95, {
      opacity: 0,
      width: 0,
    }, {
      opacity: 1,
      width: 320,
      ease: Power1.easeOut,
    }, 0.5, 1.1)
    .staggerFromTo(`${this.webPushScreen} circle`, 0.65, {
      opacity: 0,
      attr: {
        r: 0,
      }
    }, {
      opacity: 1,
      attr: {
        r: 7,
      },
      ease: Power1.easeOut,
    }, 0.5, 1.3)
    .addCallback(() => $(this.webPushScreen).addClass(css.finished))
  }

  initPushFeatures () {
    if ($(`${this.pushFeaturesScreen}`).hasClass(css.finished) || this.pushFeaturesTween.isActive()) return

    this.pushFeaturesTween
    .to(`${this.pushFeaturesScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .staggerFromTo(`${this.pushFeaturesScreen}  .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1)
    .addCallback(() => $(this.pushFeaturesScreen).addClass(css.finished))
  }

  initAppPushScreen () {
    if ($(`${this.appPushScreen}`).hasClass(css.finished) || this.appPushTween.isActive()) return

    this.appPushTween
    .to(`${this.appPushScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.appPushScreen} h2`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .55)
    .fromTo(`${this.appPushScreen } p`, .75, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .65)
    .staggerFromTo(`${this.appPushScreen}  .info_block div`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, .2)
    .fromTo(`${this.appPushScreen} .bg`, .8, {x: -220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .staggerFromTo(`${this.appPushScreen} .nav_item`, 0.95, {
      opacity: 0,
      width: 0,
    }, {
      opacity: 1,
      width: 320,
      ease: Power1.easeOut,
    }, 0.5, 1)
    .staggerFromTo(`${this.appPushScreen} circle`, 0.65, {
      opacity: 0,
      attr: {
        r: 0,
      }
    }, {
      opacity: 1,
      attr: {
        r: 7,
      },
      ease: Power1.easeOut,
    }, 0.5, 1)
    .addCallback(() => $(this.appPushScreen).addClass(css.finished))
  }

  initAppScreen () {
    if ($(`${this.appScreen}`).hasClass(css.finished) || this.appTween.isActive()) return

    this.appTween
    .to(`${this.appScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.appScreen} h2`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .65)
    .fromTo(`${this.appScreen } p`, .75, {y: -20, opacity: 0}, {
      y: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .75)
    .fromTo(`${this.appScreen} .bg`, .8, {x: 220, opacity: 0}, {
      x: 0,
      opacity: 1,
      ease: Power1.easeOut,
    }, .15)
    .staggerFromTo(`${this.appScreen} .nav_item`, 0.95, {
      opacity: 0,
      width: 0,
    }, {
      opacity: 1,
      width: 320,
      ease: Power1.easeOut,
    }, 0.5, 1)
    .staggerFromTo(`${this.appScreen} circle`, 0.65, {
      opacity: 0,
      attr: {
        r: 0,
      }
    }, {
      opacity: 1,
      attr: {
        r: 7,
      },
      ease: Power1.easeOut,
    }, 0.5, 1)
    .addCallback(() => $(this.appScreen).addClass(css.finished))
  }

  initAppFeatures () {
    if ($(`${this.appFeaturesSreen}`).hasClass(css.finished) || this.appFeaturesTween.isActive()) return

    this.appFeaturesTween
    .to(`${this.appFeaturesSreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .staggerFromTo(`${this.appFeaturesSreen}  .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1)
    .addCallback(() => $(this.appFeaturesSreen).addClass(css.finished))
  }

  initApiScreen () {
    if ($(`${this.apiScreen}`).hasClass(css.finished) || this.apiTween.isActive()) return

    this.apiTween
    .to(`${this.apiScreen}`
      , .1, {opacity: 1, ease: Power1.easeOut}, .3)
    .fromTo(`${this.apiScreen} h2`
      , .7, {y: -20, opacity: 0}, {
        y: 0,
        opacity: 1,
        ease: Power1.easeOut,
      }, .85)
    .staggerFromTo(`${this.apiScreen}  .item`, 0.65, {
      opacity: 0,
      y: -20,
    }, {
      opacity: 1,
      y: 0,
      ease: Power1.easeOut,
    }, 0.2, 1)
    .addCallback(() => $(this.apiScreen).addClass(css.finished))
  }

  setParallax (item, pageX, speedX, pageY, speedY) {
    TweenLite.to(item, 0.5, {
      x: (item.position().left + pageX * speedX) * 0.0008,
      y: (item.position().top + pageY * speedY) * 0.0008,
      ease: Linear.ease
    })
  }
}
